# LEEME #

Está app está pensada para ayudar a los asesores en SMNYL a llevar un mejor control de su cartera de clientes.

### Planes actuales ###

* Log in
* Creación de usuarios.
* Alta de clientes a mano.

### A futuro ###

* Importar listado de clientes de excel
* Exportar datos de la aplicación
* Importar datos de la aplicación
* Recordatorios de cobro, aniversario y vencimientos.
* Mina de Oro
* Correos de felicitación o recordatorio.

### Contribuyentes ###

* Erik Ichigo
* Emanuel A.
* Daniela O.

### ¿Cuando estará lista? ###

* Algún Día.